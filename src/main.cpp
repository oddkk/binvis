#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <vector>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

static size_t skip = 0;
static size_t length = 0xffffffff;
static size_t displayWidth = 32;
static size_t displayHeight = 32;
static size_t wordWidth = 1;
static size_t offset = 0;

static unsigned int color = 0x00ffffff;
static unsigned int backgroundColor = 0x00000000;

static unsigned int win_width = 512;
static unsigned int win_height = 512;

static size_t displayTop = 0;
//static std::istream input;

static GLuint surfaceVBO;
static GLuint texture;
static GLuint shader;

static size_t numSkipped = 0;
static unsigned long long cWord = 0;
static std::vector<float> buffer;
static float* textureData;
static bool requireTextureRedraw = true;

static GLFWwindow* window;

static const char vertexShader[] = "#version 330\n\
layout(location = 0) in vec4 in_Vertex;\n\
layout(location = 1) in vec2 in_UV;\n\
out vec2 ex_UV;\n\
void main()\n\
{\n\
	ex_UV = in_UV;\n\
	gl_Position = in_Vertex;\n\
}";
static const char fragmentShader[] = "#version 330\n\
in vec2 ex_UV;\n\
uniform sampler2D u_Texture;\n\
uniform vec3 u_Color;\n\
layout(location = 0) out vec4 out_Color;\n\
void main()\n\
{\n\
	float val = texture2D(u_Texture, ex_UV).r;\n\
	out_Color = vec4(val * u_Color, 1.0);\n\
}";

void printHelp();
void parseArguments(int argc, char* argv[]);

struct argInfo {
	std::string name;
	std::string shortForm;
	std::string longForm;
	std::string description;
	std::string paramName;
	bool hasParam;
	void(*parse)(std::string param);
} arguments[] = {
	{ "Help", "-h", "--help", "Print this page", "", false,
		[](std::string) -> void { printHelp(); exit(0); } },
	{ "Word size", "", "--word", "The number of bytes in a word", "word size", true,
		[](std::string param) -> void { wordWidth = std::atoi(param.c_str()); } },
	{ "Skip",   "-s", "--skip", "The number of bytes to skip before starting to read", "skip", true,
		[](std::string param) -> void { wordWidth = std::atoi(param.c_str()); } },
	{ "Offset", "-o", "--offset", "The number of words to offset by", "offset", true,
		[](std::string param) -> void { offset = std::atoi(param.c_str()); } },
	{ "Length", "-l", "--length", "The number of bytes to read", "length", true,
		[](std::string param) -> void { length = std::atoi(param.c_str()); } },
	{ "Display width", "-dw", "--display-width", "The width of a row, in bytes", "width", true,
		[](std::string param) -> void { displayWidth = std::atoi(param.c_str()); } },
	{ "Display height", "-dh", "--display-width", "The height of a column, in bytes", "width", true,
		[](std::string param) -> void { displayHeight = std::atoi(param.c_str()); } },
	{ "Display top", "-dt", "--display-width", "The starting row", "row", true,
		[](std::string param) -> void { displayTop = std::atoi(param.c_str()); } },
//	{ "File", "-f", "--file", "The file to use as input", "file path", true,
//		[](std::string param) -> void { file = param; } },
	{ "Foreground Color", "-fc", "--foreground-color", "The color of the foreground", "color", true,
		[](std::string param) -> void { color = std::stoi(param, nullptr, 16); } },
};

#define NUM_ARGS (sizeof(arguments) / sizeof(arguments[i]))

float min(float f1, float f2) { return f1 < f2 ? f1 : f2; }
float max(float f1, float f2) { return f1 > f2 ? f1 : f2; }

void printHelp()
{
	for (int i = 0; i < NUM_ARGS; i++)
	{
		argInfo* arg = &arguments[i];
		if (!arg->hasParam)
			std::cerr << arg->shortForm << " " << arg->longForm << "\t" << arg->name << ": " << arg->description << std::endl;
		else
			std::cerr << arg->shortForm << " <" << arg->paramName << "> " << arg->longForm << " <" << arg->paramName << ">\t" << arg->name << ": " << arg->description << std::endl;
	}
}

void parseArguments(int argc, char* argv[])
{
	for (int i = 0; i < argc; i++)
	{
		for (int a = 0; a < NUM_ARGS; a++)
		{
			argInfo* arg = &arguments[a];
			if (arg->shortForm == argv[i] || arg->longForm == argv[i])
			{
				std::string paramText = "";
				if (arg->hasParam)
				{
					if (i + 1 >= argc)
					{
						std::cerr << "Missing parameter for argument '" << arg->name << "' (" << arg->paramName << ")" << std::endl;
						exit(1);
					}
					paramText = argv[++i];
				}
				arg->parse(paramText);
			}
		}
	}
}

void intToColor(unsigned int color, unsigned char& r, unsigned char& g, unsigned char& b)
{
	r = (color >> 16) & 0xff;
	g = (color >> 8)  & 0xff;
	b =  color        & 0xff;
}

GLuint compileShader(const char* text, const int size, GLenum type)
{
	GLuint sh;
	sh = glCreateShader(type);
	glShaderSource(sh, 1, &text, &size);
	glCompileShader(sh);

	GLint compileStatus;
	glGetObjectParameterivARB(sh, GL_OBJECT_COMPILE_STATUS_ARB, &compileStatus);
	if (compileStatus == GL_FALSE)
	{
		std::string shaderType;
		switch(type)
		{
			case GL_VERTEX_SHADER:
				shaderType = "Vertex";
				break;
			case GL_GEOMETRY_SHADER:
				shaderType = "Geometry";
				break;
			case GL_FRAGMENT_SHADER:
				shaderType = "Fragment";
				break;
		}
		GLint len;
		glGetShaderiv(sh, GL_INFO_LOG_LENGTH, &len);
		if(len > 0)
		{
			GLchar* str = new GLchar[len];
			glGetShaderInfoLog(sh, len, nullptr, str);
			std::cerr << "Failed to compile shader of type " << shaderType << ": " << str << std::endl;
			delete[] str;
		}
		else
		{
			std::cerr << "Failed to compile shader of type " << shaderType << std::endl;
		}
		exit(1);
	}
	return sh;
}

void initOGL()
{
	const float surfaceVertex[] {
		-1.0f, -1.0f, 0.0f, 0.0f, 1.0f,
		 1.0f, -1.0f, 0.0f, 1.0f, 1.0f,
		 1.0f,  1.0f, 0.0f, 1.0f, 0.0f,
		 1.0f,  1.0f, 0.0f, 1.0f, 0.0f,
		-1.0f,  1.0f, 0.0f, 0.0f, 0.0f,
		-1.0f, -1.0f, 0.0f, 0.0f, 1.0f,
	};

	if (glewInit() != GLEW_OK)
	{
		std::cerr << "Could not initialize glew" << std::endl;
		exit(1);
	}
	std::cerr << glGetString(GL_VERSION) << std::endl;

	glGenBuffers(1, &surfaceVBO);
	glBindBuffer(GL_ARRAY_BUFFER, surfaceVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(surfaceVertex), surfaceVertex, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), 0);

	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));

	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	GLuint vshader, fshader;

	vshader = compileShader(vertexShader, sizeof(vertexShader), GL_VERTEX_SHADER);
	fshader = compileShader(fragmentShader, sizeof(fragmentShader), GL_FRAGMENT_SHADER);

	shader = glCreateProgram();
	glAttachShader(shader, vshader);
	glAttachShader(shader, fshader);

	glLinkProgram(shader);
	GLint linkStatus;
	glGetObjectParameterivARB(shader, GL_OBJECT_LINK_STATUS_ARB, &linkStatus);

	if(linkStatus == GL_FALSE)
	{
		GLint len;
		glGetProgramiv(shader, GL_INFO_LOG_LENGTH, &len);

		if(len > 0)
		{
			GLchar* str = new GLchar[len];
			glGetProgramInfoLog(shader, len, nullptr, str);
			std::cerr << "Failed to link shader: " << str << std::endl;
			delete[] str;
		}
		else
		{
			std::cerr << "Failed to link shader" << std::endl;
		}
		exit(1);
	}

	glUseProgram(shader);
	unsigned char r, g, b;
	intToColor(color, r, g, b);
	glUniform3f(glGetUniformLocation(shader, "u_Color"), (float)r / 255.0f, (float)g / 255.0f, (float)b / 255.0f);
}

void updateTexture()
{
	memset(textureData, 0, sizeof(float) * displayWidth * displayHeight);
	memcpy(textureData, &buffer[displayWidth * displayTop + offset], min(displayWidth * displayHeight * sizeof(float), (buffer.size() - displayWidth * displayTop) * sizeof(float)));
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, displayWidth, displayHeight, 0, GL_RED, GL_FLOAT, textureData);
}

void renderSurface()
{
	glClear(GL_COLOR_BUFFER_BIT);
	glUseProgram(shader);
	glBindBuffer(GL_ARRAY_BUFFER, surfaceVBO);
	glDrawArrays(GL_TRIANGLES, 0, 6);
}

void updateBuffer()
{
	if (std::cin.eof()) return;
	unsigned char c;
	while (!std::cin.eof())
	{
		c = std::cin.get();
		if (numSkipped < skip)
		{
			numSkipped++;
			continue;
		}
		buffer.push_back((float)c / 255.0f);
		if (buffer.size() > (displayTop + displayHeight) * displayWidth)
			requireTextureRedraw = true;
	}
}

void key_callback(GLFWwindow* win, int key, int scancode, int action, int mods)
{
	if (action != GLFW_RELEASE)
	{
		int d = 1;
		if ((mods & GLFW_MOD_SHIFT) != 0) d = 10;
		switch (key)
		{
			case GLFW_KEY_J:
			case GLFW_KEY_DOWN:
				if ((mods & GLFW_MOD_CONTROL) == 0)
				{
					displayTop += d;
				}
				else
				{
					size_t fb = displayTop * displayWidth;
					displayHeight += d;
					displayTop = fb / displayWidth;
					delete[] textureData;
					textureData = new float[displayWidth * displayHeight];
				}
				break;
			case GLFW_KEY_K:
			case GLFW_KEY_UP:
				if ((mods & GLFW_MOD_CONTROL) == 0)
				{
					if (displayTop >= d) displayTop -= d;
				}
				else
				{
					size_t fb = displayTop * displayWidth;
					displayHeight -= d;
					displayTop = fb / displayWidth;
					delete[] textureData;
					textureData = new float[displayWidth * displayHeight];
				}
				break;
			case GLFW_KEY_H:
			case GLFW_KEY_LEFT:
				if ((mods & GLFW_MOD_CONTROL) == 0)
				{
					offset += d;
				}
				else
				{
					size_t fb = displayTop * displayWidth;
					displayWidth -= d;
					displayTop = fb / displayWidth;
					delete[] textureData;
					textureData = new float[displayWidth * displayHeight];
				}
				break;
			case GLFW_KEY_L:
			case GLFW_KEY_RIGHT:
				if ((mods & GLFW_MOD_CONTROL) == 0)
				{
					offset -= d;
				}
				else
				{
					size_t fb = displayTop * displayWidth;
					displayWidth += d;
					displayTop = fb / displayWidth;
					delete[] textureData;
					textureData = new float[displayWidth * displayHeight];
				}
				break;
		}
	}
	requireTextureRedraw = true;
}

int main(int argc, char* argv[])
{
	parseArguments(argc, argv);
	textureData = new float[displayWidth * displayHeight];
	memset(textureData, 0, sizeof(float) * displayWidth * displayHeight);

	unsigned char bg_r, bg_g, bg_b;
	intToColor(backgroundColor, bg_r, bg_g, bg_b);
	glClearColor((float)bg_r / 255.0f, (float)bg_g / 255.0f, (float)bg_b / 255.0f, 1.0f);

	if (!glfwInit())
	{
		std::cerr << "Failed to initialize glfw" << std::endl;
		exit(1);
	}

	// Create window
	window = glfwCreateWindow(win_width, win_height, "binvis", NULL, NULL);
	glfwMakeContextCurrent(window);
	glfwSetKeyCallback(window, key_callback);

	initOGL();

	// Main loop
	while (!glfwWindowShouldClose(window))
	{
		updateBuffer();
		if (requireTextureRedraw)
		{
			updateTexture();
			renderSurface();
			glfwSwapBuffers(window);
		}
		glfwPollEvents();
	}

	glfwTerminate();
	delete[] textureData;
}

