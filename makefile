CC=clang++

CFLAGS=-c -g -Wall -std=c++11
SOURCEDIR=./src/
OUTDIR=./build/
SOURCES=$(shell find . -name *.cpp)
OBJECTS=$(SOURCES:$(SOURCEDIR)%.cpp=$(OUTDIR)%.o)
OUTPUT=$(OUTDIR)binvis

all: $(OUTPUT)

.PHONY: all

clear:
	rm -rf $(OUTDIR)

.PHONY: clear

rebuild: clear $(OUTPUT)

.PHONY: rebuild

relink: rmoutput $(OUTPUT)

.PHONY: relink

rmoutput:
	rm $(OUTPUT)

.PHONY: rmoutput

$(OUTPUT): $(OBJECTS)
	$(CC) $(OBJECTS) -std=c++11 -lutility -lwebp -lpthread -lX11 -lGL -lGLEW -lglfw -lXxf86vm -o $(OUTPUT)

$(OUTDIR)%.o:$(SOURCEDIR)%.cpp
	mkdir -p $(dir $@)
	$(CC) $(CFLAGS) $< -o $@

